from flask import (
    Flask,
    flash,
    url_for,
    redirect,
    request, 
    render_template
)

app = Flask(__name__)
app.secret_key = 'team.tasyon'

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/user/login', methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template('form.html')
#form data
    name = request.form['username']
    password = request.form['password']
    remember =request.form['remember'] if 'remember' in request.form else '0'
    access = request.form['access']

    
    task = request.args.get('task')
    
    if task == "display":
        return redirect( url_for('data', name=name, password=password, access=access, remember=remember ))
    elif task == "store":
        flash('Your data was saved successfully!!')
        return redirect( url_for('success', task=task))
        

@app.route('/success')
def success():
    return render_template('success.html')

@app.route('/data')
def data():
    return render_template('data.html')


if __name__ == '__main__':
    app.run()